/*Задача №1 вывести четные числа от 1 до 100*/

for (i=1; i<=100; i++){
    if (i%2==0){console.log(i)}
}

/*Задача №2 Возраст*/

var age=60;
if (age>=0&&age<=17) {console.log("вы слишком молоды")}
if (age>=18&&age<=60) {console.log("отличный возраст")}
if (age>=61) {console.log("привет, бабуля!")}

/*Задача №3 Дата*/

var date="08.03";
switch (date){
    case "08.03": console.log("с 8 марта!"); break;
    case "23.02": console.log("с 23 февраля"); break;
    case "31.12": console.log("с новым годом"); break;
    default: console.log("сегодня просто отличный день и безо всякого праздника"); break;
}